package sprout.jstudy.activemq.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;
import sprout.jstudy.activemq.MQType;

import java.util.HashMap;
import java.util.Map;

/**
 * MQ消息发送
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-2
 * Time: 下午6:20
 * To change this template use File | Settings | File Templates.
 */
public class MQSender {

    private static final Log log = LogFactory.getLog(MQSender.class);
    private JmsTemplate jmsTemplate;

    /**
     * 发送消息到消息队列
     * @param mqType
     * @param params
     */
    public void send(final MQType mqType, final Map.Entry<String, Object>... params) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        for(Map.Entry<String, Object> param : params){
            paramMap.put(param.getKey(), param.getValue());
        }

        Map<String, Object> message = new HashMap<String, Object>();
        message.put("type", mqType);
        message.put("params", paramMap);

        jmsTemplate.convertAndSend(message);
        log.info("MQ消息发送成功");
    }

    /**
     * 发送消息到消息队列
     * @param mqType
     * @param params
     */
    public void send(final MQType mqType, final Map<String, Object> params){

    }

    public JmsTemplate getJmsTemplate() {
        return jmsTemplate;
    }

    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }
}

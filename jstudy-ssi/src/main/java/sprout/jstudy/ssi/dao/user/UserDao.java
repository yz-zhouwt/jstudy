package sprout.jstudy.ssi.dao.user;

import sprout.jstudy.ssi.domain.user.User;
import sprout.jstudy.ssi.domain.user.UserQuery;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-16
 * Time: 下午2:46
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao {

    public boolean add(User user);

    public boolean update(User user);

    public User getById(long id);

    public int count(UserQuery userQuery);

    public List<User> list(UserQuery userQuery);

}

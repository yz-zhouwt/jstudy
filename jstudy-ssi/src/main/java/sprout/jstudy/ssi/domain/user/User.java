package sprout.jstudy.ssi.domain.user;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-16
 * Time: 下午2:39
 * To change this template use File | Settings | File Templates.
 * 建表语句：
 * CREATE TABLE `user` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `name` varchar(20) NOT NULL,
 `sex` tinyint(2) NOT NULL,
 `age` tinyint(3) NOT NULL,
 `email` varchar(100) DEFAULT NULL,
 PRIMARY KEY (`id`)
 )
 */
public class User {

    private long id;
    private String name;
    private int sex;
    private int age;
    private String email;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
